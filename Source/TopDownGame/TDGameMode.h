// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TDGameMode.generated.h"

UCLASS(minimalapi)
class ATDGameMode : public AGameMode
{
	GENERATED_BODY()

public:
    ATDGameMode(const FObjectInitializer& ObjectInitializer);

private:
	virtual void RestartPlayer(class AController* NewPlayer) override;
	virtual void HandleMatchHasStarted() override;
};



