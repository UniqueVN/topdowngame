// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDControlPoint.h"


ATDControlPoint::ATDControlPoint(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	ControlRadius(1000.f)
{
	ControlSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ControlSphere"));
	ControlSphere->InitSphereRadius(ControlRadius);
	//ControlSphere->RelativeLocation.Z = -CollisionComponent->GetScaledBoxExtent().Z;
	ControlSphere->AttachTo(RootComponent);
}

const Route* ATDControlPoint::FindRouteTo(ATDControlPoint* neighbor) const
{
	for (int32 i = 0; i < Neighbors.Num(); i++)
	{
		if (Neighbors[i] == neighbor)
		{
			return i < RouteToNeighbors.Num() ? RouteToNeighbors[i].Get() : nullptr;
		}
	}
	return nullptr;
}

void ATDControlPoint::BeginPlay()
{
	Super::BeginPlay();

	UNavigationSystem* navSys = GetWorld()->GetNavigationSystem();
	FNavAgentProperties agentProps(500.f, 200.f);
	if (navSys)
	{
		RouteToNeighbors.Reserve(Neighbors.Num());
		for (const ATDControlPoint* neighbor : Neighbors)
		{
			RoutePtr route;
			const Route* otherRoute = neighbor->FindRouteTo(this);
			if (otherRoute != nullptr)
			{
				route = otherRoute->MakeReverse();
			}
			else
			{
				FPathFindingQuery query(this, *navSys->GetNavDataForProps(agentProps), GetActorLocation(), neighbor->GetActorLocation());
				FPathFindingResult result = navSys->FindPathSync(agentProps, query);

				if (result.Result == ENavigationQueryResult::Success)
				{
					route = RoutePtr(new Route(*result.Path));
					route->Trim(ControlRadius, true);
					route->Trim(neighbor->GetControlRadius(), false);

					for (int32 i = 0; i < route->GetWaypoints().Num() - 1; i++)
					{
						DrawDebugLine(GetWorld(), route->GetWaypoints()[i] + FVector(0, 0, 30), route->GetWaypoints()[i + 1] + FVector(0, 0, 30), FColor::Red, true);
					}
				}
				else
				{
					UE_LOG(LogTopDownGame, Error, TEXT("Failed to find path between %s and %s."), *GetName(), *neighbor->GetName());
				}
			}

			RouteToNeighbors.Add(route);
		}
	}
}

#if WITH_EDITOR
void ATDControlPoint::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (!PropertyChangedEvent.Property)
		return;

	if (PropertyChangedEvent.Property->GetName() == TEXT("ControlRadius"))
	{
		ControlSphere->SetSphereRadius(ControlRadius);
	}
}

void ATDControlPoint::EditorApplyScale(const FVector& DeltaScale, const FVector* PivotLocation, bool bAltDown, bool bShiftDown, bool bCtrlDown)
{
	float MaxScale = DeltaScale.GetMax();
	float MinScale = DeltaScale.GetMin();
	float DeltaRadius = (FMath::Abs(MaxScale) >= FMath::Abs(MinScale) ? MaxScale : MinScale) * 100.f;

	ControlRadius = FMath::Max(100.f, ControlRadius + DeltaRadius);
	ControlSphere->SetSphereRadius(ControlRadius);

	PostEditChange();
}

#endif

Route::Route()
{
}

Route::Route(const FNavigationPath& path)
{
	const TArray<FNavPathPoint>& pathPoints = path.GetPathPoints();
	Waypoints.SetNum(pathPoints.Num());
	for (int32 i = 0; i < pathPoints.Num(); i++)
	{
		Waypoints[i] = pathPoints[i].Location;
	}
}

const TArray<FVector>& Route::GetWaypoints() const
{
	return Waypoints;
}

RoutePtr Route::MakeReverse() const
{
	RoutePtr reversed(new Route());
	reversed->Waypoints.Reserve(Waypoints.Num());
	for (int32 i = Waypoints.Num() - 1; i >= 0; i--)
	{
		reversed->Waypoints.Add(Waypoints[i]);
	}
	return reversed;
}

void Route::Trim(float dist, bool fromStart)
{
	if (dist <= 0.f || Waypoints.Num() < 2)
		return;

	const FVector& start = fromStart ? Waypoints[0] : Waypoints[Waypoints.Num()-1];
	float distSq = dist * dist;
	for (int32 i = 1; i < Waypoints.Num(); i++)
	{
		const FVector& current = fromStart ? Waypoints[i] : Waypoints[Waypoints.Num() - 1 - i];
		if ((current - start).SizeSquared() > distSq)
		{
			if (i == 1)
				return;

			FVector& prev = fromStart ? Waypoints[i - 1] : Waypoints[Waypoints.Num() - i];
			FVector dir;
			float length;
			(current - prev).ToDirectionAndLength(dir, length);
			const float toMid = (dir | (start - prev));
			const FVector mid = prev + dir * toMid;
			const float discSq = dist * dist - (mid - start).SizeSquared();

			if (discSq >= 0.f)
			{
				const float disc = FMath::Sqrt(discSq);
				if (toMid - disc >= 0.f)
				{
					// cut up to prev, and replace prev with mid - dir * disc
					prev = mid - dir * disc;
				}
				else if (toMid + disc >= 0.f)
				{
					// cut up to prev, and replace prev with mid + dir * disc
					prev = mid + dir * disc;
				}
			}

			if (i > 2)
			{
				if (fromStart)
				{
					Waypoints.RemoveAt(1, i - 2);
				}
				else
				{
					Waypoints.RemoveAt(Waypoints.Num() - i + 1, i - 2);
				}
			}

			return;
		}
	}

	// cut anything in between start and end
	if (Waypoints.Num() > 2)
	{
		Waypoints.RemoveAt(1, Waypoints.Num() - 2);
	}
}