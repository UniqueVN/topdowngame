// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDFactory.h"
#include "OOB/TDRoster.h"
#include "OOB/TDUnit.h"

ATDFactory::ATDFactory(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, currentUnitSet(0)
	, currentUnitCount(0)
{
}

bool ATDFactory::BuildNextUnit()
{
	UTDRoster* defaultRoster = Cast<UTDRoster>(Roster->GetDefaultObject());
	if (defaultRoster == nullptr)
		return false;

	if (currentUnitSet >= defaultRoster->GetUnits().Num())
		return false;

	const FRosterUnitInfo& unitSet = defaultRoster->GetUnits()[currentUnitSet];
	if (currentUnitCount >= unitSet.Count)
		return false;

	USceneComponent* point = GetUnitSpawnPoint();
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	params.bNoFail = true;
	auto unit = GetWorld()->SpawnActor<ATDUnit>(*unitSet.Prototype, point->GetComponentLocation(), point->GetComponentRotation(), params);

	currentUnitCount++;
	if (currentUnitCount >= unitSet.Count)
		currentUnitSet++;

	return currentUnitSet < defaultRoster->GetUnits().Num();
}