// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TDBuilding.h"
#include "TDControlPoint.generated.h"

class Route;
typedef TSharedPtr<Route, ESPMode::ThreadSafe> RoutePtr;

class TOPDOWNGAME_API Route
{
public:
	Route();
	explicit Route(const FNavigationPath& path);
	FORCEINLINE const TArray<FVector>& GetWaypoints() const;
	RoutePtr MakeReverse() const;
	void Trim(float dist, bool fromStart);

private:
	TArray<FVector> Waypoints;
};

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNGAME_API ATDControlPoint : public ATDBuilding
{
	GENERATED_BODY()
	
public:
	ATDControlPoint(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleDefaultsOnly)
	class USphereComponent* ControlSphere;

	UPROPERTY(EditAnywhere, Category = ControlPoint)
	float ControlRadius;

	UPROPERTY(EditInstanceOnly, Category = ControlPoint)
	TArray<ATDControlPoint*> Neighbors;

public:
	FORCEINLINE float GetControlRadius() const { return ControlRadius; }

	const Route* FindRouteTo(ATDControlPoint* neighbor) const;

private:
	virtual void BeginPlay() override;

protected:
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	virtual void EditorApplyScale(const FVector& DeltaScale, const FVector* PivotLocation, bool bAltDown, bool bShiftDown, bool bCtrlDown) override;
#endif	

private:
	TArray<RoutePtr> RouteToNeighbors;
};
