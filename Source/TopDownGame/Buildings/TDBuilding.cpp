// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TopDownGame.h"
#include "Blueprint/UserWidget.h"
#include "Components/BillboardComponent.h"
#include "Components/WidgetComponent.h"
#include "UI/TDHUDIndicator.h"
#include "TDBuilding.h"

ATDBuilding::ATDBuilding(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer),
	StartingOwnerIndex(-1)
{
    //CollisionComponent = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("CollisionComponent"));
    //CollisionComponent->SetBoxExtent(FVector(100, 100, 100));
    //RootComponent = CollisionComponent;

	RootComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("RootComponent"));

    MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MeshComponent"));
    MeshComponent->AlwaysLoadOnClient = true;
    MeshComponent->AlwaysLoadOnServer = true;
    MeshComponent->AttachTo(RootComponent);
    MeshComponent->SetAbsolute(false, false, true);

	//RootComponent = MeshComponent;

    InfoComponent = ObjectInitializer.CreateDefaultSubobject<UMaterialBillboardComponent>(this, TEXT("InfoComponent"));
    InfoComponent->AttachTo(RootComponent);

    InfoWidgetComponent = ObjectInitializer.CreateDefaultSubobject<UWidgetComponent>(this, TEXT("InfoWidgetComponent"));
    InfoWidgetComponent->AttachTo(RootComponent);

    UnitSpawnPoint = ObjectInitializer.CreateDefaultSubobject<UArrowComponent>(this, TEXT("UnitSpawnPoint"));
    UnitSpawnPoint->AttachTo(RootComponent);
}

void ATDBuilding::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    UpdateHudWidget();
}

/*void ATDBuilding::PostActorCreated()
{
	Super::PostActorCreated();

	ChangeState(NAME_CreatedInGame);
}*/

void ATDBuilding::BeginPlay()
{
	Super::BeginPlay();

// 	if (IsInState(NAME_None))
// 	{
// 		BeginActive();
// 	}
// 	else if (IsInState(NAME_CreatedInGame))
// 	{
// 		BecomeInvisible();
// 	}
}

int32 ATDBuilding::GetStartingOwnerIndex()
{
	return StartingOwnerIndex;
}

void ATDBuilding::BecomeInvisible()
{
	if (!IsInState(NAME_Invisible))
	{
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		ChangeState(NAME_Invisible);
	}
}

void ATDBuilding::BecomeHologram()
{
	if (IsInState(NAME_Invisible))
	{
		SetActorHiddenInGame(false);
		SetActorEnableCollision(false);
		ChangeState(NAME_Hologram);
	}
}

void ATDBuilding::BeginConstruction()
{
	if (IsReadyToConstruct())
	{
		SetActorEnableCollision(true);
		SetActorHiddenInGame(false);
		BecomeActive();
	}
}

void ATDBuilding::BecomeActive()
{
	ChangeState(NAME_Active);
	BuildingBecomeActive();
}

bool ATDBuilding::IsReadyToConstruct()
{
	return IsInState(NAME_Hologram);
}

FName ATDBuilding::NAME_CreatedInGame("CreatedInGame");
FName ATDBuilding::NAME_Invisible("Invisible");
FName ATDBuilding::NAME_Hologram("Hologram");
FName ATDBuilding::NAME_Active("Active");

bool ATDBuilding::IsInState(FName stateName)
{
	return StateName == stateName;
}

void ATDBuilding::ChangeState(FName stateName)
{
	StateName = stateName;
}

#if WITH_EDITOR
void ATDBuilding::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    Super::PostEditChangeProperty(PropertyChangedEvent);

    if (!PropertyChangedEvent.Property)
        return;

    if (PropertyChangedEvent.Property->GetName() == TEXT("DisplayName"))
    {
        UpdateHudWidget();
    }

    if (PropertyChangedEvent.Property->GetName() == TEXT("HUDWidgetClass"))
    {
        UpdateHudWidget();
    }
}
#endif

void ATDBuilding::UpdateHudWidget()
{
    //InfoWidgetComponent->WidgetClass = HUDWidgetClass;
    InfoWidgetComponent->InitWidget();
    HUDWidget = InfoWidgetComponent->GetUserWidgetObject();

    UpdateHudContent();
}

void ATDBuilding::UpdateHudContent()
{
    UTDHUDIndicator* HUDIndicator = Cast<UTDHUDIndicator>(HUDWidget);
    if (!HUDIndicator)
        return;
    
    HUDIndicator->SetDisplayName(DisplayName);
}

UArrowComponent* ATDBuilding::GetUnitSpawnPoint() const
{
    return UnitSpawnPoint;
}

FConstructionOutput ATDBuilding::GenerateConstruction(const TArray<FConstructionSlot>& Queue, float DeltaTime) const
{
	FConstructionOutput output;
	output.SlotIndex = 0;
	output.Progress = 1.f * DeltaTime;
	return output;
}

void ATDBuilding::Focus(ATDPlayerController* player)
{
	if (MeshComponent != NULL)
	{
		MeshComponent->SetRenderCustomDepth(true);
	}
}

void ATDBuilding::Unfocus(ATDPlayerController* player)
{
	if (MeshComponent != NULL)
	{
		MeshComponent->SetRenderCustomDepth(false);
	}
}

