// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Buildings/TDBuilding.h"
#include "TDFactory.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API ATDFactory : public ATDBuilding
{
	GENERATED_BODY()
	
public:
	ATDFactory(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UTDRoster> Roster;

	UFUNCTION(BlueprintCallable, Category = Building)
	bool BuildNextUnit();

private:
	int currentUnitSet;
	int currentUnitCount;
	
};
