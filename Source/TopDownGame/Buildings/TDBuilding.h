// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Actor.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "TDPlayerController.h"
#include "TDBuilding.generated.h"

struct FConstructionOutput
{
	FConstructionOutput() : SlotIndex(-1), Progress(0.f) {}

	int32 SlotIndex;
	float Progress;
};

UCLASS(Blueprintable)
class ATDBuilding : public AActor, public IFocusable
{
    GENERATED_BODY()

public:
    ATDBuilding(const FObjectInitializer& ObjectInitializer);

public: // Components
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	//class UBoxComponent* CollisionComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UMaterialBillboardComponent* InfoComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* InfoWidgetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* UnitSpawnPoint;

public: // Properties
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Content, meta = (AllowPrivateAccess = "true"))
	FString DisplayName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = "true"))
	UUserWidget* HUDWidget;

private:
	UPROPERTY(EditInstanceOnly, Category = Building, meta = (AllowPrivateAccess = "true"))
	int32 StartingOwnerIndex;

public:
	int32 GetStartingOwnerIndex();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


private:
	// Allow actors to initialize themselves on the C++ side
	virtual void PostInitializeComponents() override;

	//virtual void PostActorCreated() override;

	virtual void Focus(ATDPlayerController* player) override;
	virtual void Unfocus(ATDPlayerController* player) override;

public:	
	void BecomeInvisible();
	void BecomeHologram();
	void BeginConstruction();	
	void BecomeActive();

	bool IsReadyToConstruct();

private:

	static FName NAME_CreatedInGame;
	static FName NAME_Invisible;
	static FName NAME_Hologram;
	static FName NAME_Active;

	bool IsInState(FName stateName);
	void ChangeState(FName stateName);

public:
    //UFUNCTION(BlueprintImplementableEvent, Category = Building)
    //void UpdateInfoComponent();

	UFUNCTION(BlueprintImplementableEvent, Category = Building)
	void BuildingBecomeActive();

	FConstructionOutput GenerateConstruction(const TArray<FConstructionSlot>& Queue, float DeltaTime) const;
    UArrowComponent* GetUnitSpawnPoint() const;

protected:
    void UpdateHudWidget();
    void UpdateHudContent();

	// Editor support
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	FName StateName;
};

