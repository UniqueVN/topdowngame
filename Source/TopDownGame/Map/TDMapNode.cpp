// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDMapNode.h"


// Sets default values
ATDMapNode::ATDMapNode() :
	Radius(1000.f)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SphereOfInfluence = CreateDefaultSubobject<USphereComponent>(TEXT("SphereOfInfluence"));
	SphereOfInfluence->InitSphereRadius(Radius);

	RootComponent = SphereOfInfluence;

}

// Called when the game starts or when spawned
void ATDMapNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDMapNode::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

}

#if WITH_EDITOR
void ATDMapNode::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (!PropertyChangedEvent.Property)
		return;

	if (PropertyChangedEvent.Property->GetName() == TEXT("Radius"))
	{
		SphereOfInfluence->SetSphereRadius(Radius);
	}
}

void ATDMapNode::EditorApplyScale(const FVector& DeltaScale, const FVector* PivotLocation, bool bAltDown, bool bShiftDown, bool bCtrlDown)
{
	float MaxScale = DeltaScale.GetMax();
	float MinScale = DeltaScale.GetMin();
	float DeltaRadius = (FMath::Abs(MaxScale) >= FMath::Abs(MinScale) ? MaxScale : MinScale) * 100.f;

	Radius = FMath::Max(100.f, Radius + DeltaRadius);
	SphereOfInfluence->SetSphereRadius(Radius);

	PostEditChange();
}

#endif

