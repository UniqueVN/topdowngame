// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TDMapNode.generated.h"

UCLASS()
class TOPDOWNGAME_API ATDMapNode : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY()
	class USphereComponent* SphereOfInfluence;

	UPROPERTY(EditAnywhere, Category = Map)
	float Radius;

public:	
	// Sets default values for this actor's properties
	ATDMapNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;	

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	virtual void EditorApplyScale(const FVector& DeltaScale, const FVector* PivotLocation, bool bAltDown, bool bShiftDown, bool bCtrlDown) override;
#endif
};
