// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDSpectatorMovement.h"
#include "TDPlayerController.h"

UTDSpectatorMovement::UTDSpectatorMovement(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer), bInitialLocationSet(false)
{
	MaxSpeed = 80000.f;
	Acceleration = 80000.f;
	Deceleration = 40000.f;
}


void UTDSpectatorMovement::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent)
	{
		return;
	}
	
	auto PlayerController = Cast<ATDPlayerController>(PawnOwner->GetController());
	if (PlayerController && PlayerController->IsLocalController())
	{
		if (!bInitialLocationSet)
		{
			PawnOwner->SetActorRotation(PlayerController->GetControlRotation());
			PawnOwner->SetActorLocation(PlayerController->GetSpawnLocation());
			bInitialLocationSet = true;
		}

		FVector MyLocation = UpdatedComponent->GetComponentLocation();

// 		ATDSpectatorPawn* SpectatorPawn = Cast<ATDSpectatorPawn>(PawnOwner/*PlayerController->GetSpectatorPawn()*/);
// 		if ((SpectatorPawn != NULL) && (SpectatorPawn->GetSpectatorCameraComponent() != NULL))
// 		{
// 			SpectatorPawn->GetSpectatorCameraComponent()->ClampCameraLocation(PlayerController, MyLocation);
// 		}
		UpdatedComponent->SetWorldLocation(MyLocation, false);
	}
}




