// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SpectatorPawnMovement.h"
#include "TDSpectatorMovement.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API UTDSpectatorMovement : public USpectatorPawnMovement
{
	GENERATED_BODY()

public:
	UTDSpectatorMovement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

private:
	bool bInitialLocationSet;
};
