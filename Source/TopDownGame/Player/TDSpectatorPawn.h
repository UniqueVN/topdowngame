// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SpectatorPawn.h"
#include "TDSpectatorPawn.generated.h"

class UTDSpectatorCameraComponent;

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API ATDSpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

public:

	ATDSpectatorPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
public:
	/* Returns a pointer to the strategy camera component the pawn has. */
	UTDSpectatorCameraComponent* GetSpectatorCameraComponent();

private:
	/** Handles the mouse scrolling down. */
	void OnMouseScrollUp();

	/** Handles the mouse scrolling up. */
	void OnMouseScrollDown();

	// Begin ADefaultPawn interface
	
	/** event call on move forward input */
	virtual void MoveForward(float Val) override;

	/** event call on strafe right input */
	virtual void MoveRight(float Val) override;

	/** Add custom key bindings */
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// End ADefaultPawn interface

private:

	// The camera component for this camera
	UPROPERTY(Category = CameraActor, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UTDSpectatorCameraComponent* SpectatorCameraComponent;
};
