// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDSpectatorPawn.h"
#include "TDSpectatorCameraComponent.h"
#include "TDSpectatorMovement.h"

ATDSpectatorPawn::ATDSpectatorPawn(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UTDSpectatorMovement>(Super::MovementComponentName))
{
	GetCollisionComponent()->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	bAddDefaultMovementBindings = false;
	SpectatorCameraComponent = ObjectInitializer.CreateDefaultSubobject<UTDSpectatorCameraComponent>(this, TEXT("SpectatorCameraComponent"));
}

void ATDSpectatorPawn::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	check(InputComponent);

	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &ATDSpectatorPawn::OnMouseScrollUp);
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &ATDSpectatorPawn::OnMouseScrollDown);

	InputComponent->BindAxis("MoveForward", this, &ATDSpectatorPawn::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATDSpectatorPawn::MoveRight);
}

void ATDSpectatorPawn::OnMouseScrollUp()
{
	SpectatorCameraComponent->OnZoomIn();
}

void ATDSpectatorPawn::OnMouseScrollDown()
{
	SpectatorCameraComponent->OnZoomOut();
}

void ATDSpectatorPawn::MoveForward(float Val)
{
	SpectatorCameraComponent->MoveForward(Val);
}

void ATDSpectatorPawn::MoveRight(float Val)
{
	SpectatorCameraComponent->MoveRight(Val);
}

UTDSpectatorCameraComponent* ATDSpectatorPawn::GetSpectatorCameraComponent()
{
	check(SpectatorCameraComponent != NULL);
	return SpectatorCameraComponent;
}

