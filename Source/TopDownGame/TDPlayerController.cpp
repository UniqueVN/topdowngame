// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TopDownGame.h"
#include "TDPlayerController.h"
#include "Buildings/TDBuilding.h"
#include "Buildings/TDControlPoint.h"
#include "AI/Navigation/NavigationSystem.h"
#include "OOB/TDRoster.h"
#include "OOB/TDSquad.h"
#include "OOB/TDUnit.h"
#include "Player/TDSpectatorPawn.h"
#include "Player/TDSpectatorCameraComponent.h"
#include "Input/TDInputContext.h"
#include "Input/TDInputContext_Default.h"
#include "Input/TDInputContext_PlaceBuilding.h"

ATDPlayerController::ATDPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

// Called when the game starts or when spawned
void ATDPlayerController::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<ATDBuilding> BuildingIt(GetWorld()); BuildingIt; ++BuildingIt)
	{
		Factories.Add(*BuildingIt);
	}

	AddInputContext<UTDInputContext_Default>();
}

void ATDPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	UpdateInputContext(DeltaTime);

	UpdateConstruction(DeltaTime);

	UpdateFocus();

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void ATDPlayerController::ProcessPlayerInput(const float DeltaTime, const bool bGamePaused)
{
	Super::ProcessPlayerInput(DeltaTime, bGamePaused);

	ATDSpectatorPawn* spectator = Cast<ATDSpectatorPawn>(GetSpectatorPawn());
	if (spectator != NULL)
	{
		spectator->GetSpectatorCameraComponent()->UpdateCameraMovement(this);
	}
}

void ATDPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("Select", IE_Pressed, this, &ATDPlayerController::OnSelect);

	//InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATDPlayerController::OnSetDestinationPressed);
	//InputComponent->BindAction("SetDestination", IE_Released, this, &ATDPlayerController::OnSetDestinationReleased);

	// support touch devices 
	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATDPlayerController::MoveToTouchLocation);
	//InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ATDPlayerController::MoveToTouchLocation);
}

void ATDPlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(Hit.ImpactPoint);
	}
}

void ATDPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void ATDPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const Pawn = GetPawn();
	if (Pawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, Pawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void ATDPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void ATDPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

template<class THandler>
void ATDPlayerController::ProcessInputContextAction(const THandler& handler, bool canInterrupt)
{
	auto tempStack = InputContextStack;
	for (int32 i = tempStack.Num() - 1; i >= 0; i--)
	{
		UTDInputContext* context = tempStack[i];
		InputContextResult result = handler.Process(*context);
		if (result & InputContextResult::Finished)
		{
			RemoveInputContext(context);
		}

		if (canInterrupt && (result & InputContextResult::Handled))
		{
			break;
		}
	}
}

class UpdateHandler
{
public:
	UpdateHandler(float deltaTime):
		DeltaTime(deltaTime)
	{}

	InputContextResult Process(UTDInputContext& inputContext) const
	{
		return inputContext.Update(DeltaTime);
	}

private:
	float DeltaTime;
};

typedef InputContextResult (UTDInputContext::*InputContextAction)();

class CommonHandler
{
public:
	CommonHandler(InputContextAction action) :
		Action(action)
	{}

	InputContextResult Process(UTDInputContext& inputContext) const
	{
		return (inputContext.*Action)();
	}

private:
	InputContextAction Action;
};

void ATDPlayerController::UpdateInputContext(float deltaTime)
{
	ProcessInputContextAction(UpdateHandler(deltaTime), false);
}

void ATDPlayerController::AddInputContext(UTDInputContext* inputContext)
{
	check(inputContext != NULL);
	for (const UTDInputContext* context : InputContextStack)
	{
		if (context->GetClass() == inputContext->GetClass())
		{
			UE_LOG(LogTopDownGame, Error, TEXT("Unique context input %s is added more than once, please prevent from that type of input from happening."), *context->GetClass()->GetName());
			return;
		}
	}

	InputContextStack.Add(inputContext);
	inputContext->Activate(this);
}

void ATDPlayerController::RemoveInputContext(UTDInputContext* inputContext)
{
	check(inputContext != NULL);
	if (InputContextStack.Remove(inputContext) > 0)
	{
		inputContext->Deactivate();
	}
}

void ATDPlayerController::BeginPlaceBuilding(TSubclassOf<ATDBuilding> buildingClass)
{
	if (buildingClass == NULL)
	{
		return;
	}

	UTDInputContext_PlaceBuilding* context = NewObject<UTDInputContext_PlaceBuilding>(this);
	context->SetBuildingClass(buildingClass);
	AddInputContext(context);
}

void ATDPlayerController::AcquireBuilding(ATDBuilding* building)
{
	building->SetOwner(this);
	
	ATDControlPoint* cp = Cast<ATDControlPoint>(building);
	if (cp != NULL)
	{
		OwnedControlPoints.AddUnique(cp);
	}
}

void ATDPlayerController::OnSelect()
{
	ProcessInputContextAction(CommonHandler(&UTDInputContext::Select), true);
}

void ATDPlayerController::ConstructSquad(TSubclassOf<class UTDRoster> RosterClass)
{
	UTDRoster* DefaultRoster = Cast<UTDRoster>(RosterClass->GetDefaultObject());

	ATDSquad* Squad = GetWorld()->SpawnActor<ATDSquad>();
	Squad->SetOwner(this);
	Squad->SetRoster(DefaultRoster);
	Squad->Construct();
}

void ATDPlayerController::QueueConstruction(TSubclassOf<class ATDUnit> prototype, ATDSquad* squad, int32 count)
{
	FConstructionSlot Slot;
	Slot.Prototype = prototype;
	Slot.Squad = squad;
	Slot.Progress = 0.f;

	for (int32 i = 0; i < count; i++)
	{
		ConstructionQueue.Add(Slot);
	}
}

void ATDPlayerController::UpdateConstruction(float DeltaTime)
{
	if (ConstructionQueue.Num() > 0)
	{
		int32 maxAffected = -1;

		for (const ATDBuilding* factory : Factories)
		{
			FConstructionOutput output = factory->GenerateConstruction(ConstructionQueue, DeltaTime);
			if (output.SlotIndex >= 0)
			{
				ConstructionQueue[output.SlotIndex].Progress += output.Progress;
				maxAffected = FMath::Max(maxAffected, output.SlotIndex);
			}
		}

		for (int32 i = maxAffected; i >= 0; i--)
		{
			const FConstructionSlot& slot = ConstructionQueue[i];
			int32 totalCost = slot.Prototype->GetDefaultObject<ATDUnit>()->ConstructionCost;
			if (FMath::FloorToInt(slot.Progress) >= totalCost)
			{
				CompleteConstruction(slot.Prototype, slot.Squad);
				ConstructionQueue.RemoveAt(i);
			}
		}
	}
}

void ATDPlayerController::CompleteConstruction(TSubclassOf<class ATDUnit> prototype, ATDSquad* squad)
{
	ATDBuilding* spawnAtFactory = Factories[0];
	USceneComponent* point = spawnAtFactory->GetUnitSpawnPoint();	
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	params.bNoFail = true;
	auto unit = GetWorld()->SpawnActor<ATDUnit>(*prototype, point->GetComponentLocation(), point->GetComponentRotation(), params);
	squad->AddUnit(unit);
}

void ATDPlayerController::UpdateFocus()
{
	AActor* newFocus = FindFocus();
	
	if (Focused != newFocus)
	{
		if (Focused != NULL)
			Cast<IFocusable>(Focused)->Unfocus(this);

		Focused = newFocus;

		if (Focused != NULL)
			Cast<IFocusable>(Focused)->Focus(this);
	}
}

AActor* ATDPlayerController::FindFocus()
{
	// Trace to see what is under the mouse cursor
	FHitResult hit;
	GetHitResultUnderCursor(ECC_Visibility, false, hit);
	if (hit.bBlockingHit)
	{
		AActor* hitActor = hit.GetActor();
		if (hitActor != NULL && Cast<IFocusable>(hitActor) != NULL)
		{
			return hitActor;
		}
	}

	return NULL;
}
