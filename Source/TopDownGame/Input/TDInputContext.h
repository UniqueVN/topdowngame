// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "TDInputContext.generated.h"

/**
 * 
 */
class ATDPlayerController;

enum InputContextResult
{
	None			= 0x00,
	Finished		= 0x01,
	Handled			= 0x02,

	FinishedAndHandled = Finished | Handled,
};

UCLASS()
class TOPDOWNGAME_API UTDInputContext : public UObject
{
	GENERATED_BODY()
	
public:
	void Activate(ATDPlayerController* controller);
	void Deactivate();
	InputContextResult Update(float deltaTime);
	InputContextResult Select();

protected:

	FORCEINLINE ATDPlayerController* GetController() { return Controller; }

	virtual void OnActivated() {}
	virtual void OnDeactivated() {}
	virtual InputContextResult OnUpdate(float deltaTime) { return InputContextResult::None; }
	virtual InputContextResult OnSelect() { return InputContextResult::None; }

private:
	UPROPERTY(Transient)
	ATDPlayerController* Controller;
};
