// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Input/TDInputContext.h"
#include "TDInputContext_PlaceBuilding.generated.h"

class ATDBuilding;

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API UTDInputContext_PlaceBuilding : public UTDInputContext
{
	GENERATED_BODY()

public:
	const TSubclassOf<ATDBuilding>& GetBuildingClass();
	void SetBuildingClass(const TSubclassOf<ATDBuilding>& buildingClass);

private:
	virtual void OnActivated() override;
	virtual InputContextResult OnUpdate(float deltaTime) override;
	virtual InputContextResult OnSelect() override;

	bool FindPlacementLocation(FVector& o_loc);

	UPROPERTY(Transient)
	TSubclassOf<ATDBuilding> BuildingClass;

	UPROPERTY(Transient)
	ATDBuilding* Building;	
};
