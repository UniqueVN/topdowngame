// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDInputContext_PlaceBuilding.h"
#include "Buildings/TDBuilding.h"
#include "Buildings/TDControlPoint.h"

const TSubclassOf<ATDBuilding>& UTDInputContext_PlaceBuilding::GetBuildingClass()
{
	return BuildingClass;
}

void UTDInputContext_PlaceBuilding::SetBuildingClass(const TSubclassOf<ATDBuilding>& buildingClass)
{
	check(buildingClass != NULL);
	BuildingClass = buildingClass;
}

void UTDInputContext_PlaceBuilding::OnActivated()
{
	Building = GetController()->GetWorld()->SpawnActor<ATDBuilding>(BuildingClass);
	check(Building != NULL);
	Building->BecomeInvisible();
}

InputContextResult UTDInputContext_PlaceBuilding::OnUpdate(float deltaTime)
{
	FVector placeLoc;
	if (FindPlacementLocation(placeLoc))
	{
		//FVector placeLoc = hit.Location;
		//placeLoc.Z += Building->GetRootComponent()->GetPlacementExtent().GetBox().GetExtent().Z;
		Building->SetActorLocation(placeLoc);
		Building->BecomeHologram();
	}
	else
	{
		Building->BecomeInvisible();
	}

	return InputContextResult::None;
}

InputContextResult UTDInputContext_PlaceBuilding::OnSelect()
{
	if (Building->IsReadyToConstruct())
	{
		GetController()->AcquireBuilding(Building);
		Building->BeginConstruction();
		return InputContextResult::FinishedAndHandled;
	}

	return InputContextResult::Handled;
}

bool UTDInputContext_PlaceBuilding::FindPlacementLocation(FVector& o_loc)
{
	FHitResult hit;
	GetController()->GetHitResultUnderCursor(ECC_Placement, false, hit);
	if (hit.bBlockingHit)
	{
		for (ATDControlPoint* cp : GetController()->GetOwnedControlPoints())
		{
			check(cp != NULL);
			float radius = cp->GetControlRadius();
			if ((hit.Location - cp->GetActorLocation()).SizeSquared2D() <= radius * radius)
			{
				o_loc = hit.Location;
				return true;
			}
		}
	}

	return false;

}
