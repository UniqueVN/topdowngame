// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDInputContext.h"


void UTDInputContext::Activate(ATDPlayerController* controller)
{
	Controller = controller;
	OnActivated();
}

void UTDInputContext::Deactivate()
{
	OnDeactivated();
}

InputContextResult UTDInputContext::Update(float deltaTime)
{
	return OnUpdate(deltaTime);
}

InputContextResult UTDInputContext::Select()
{
	return OnSelect();
}

