// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "OOB/TDRoster.h"
#include "Focusable.h"
#include "TDPlayerController.generated.h"

class ATDSquad;
class UTDInputContext;
class ATDBuilding;
class ATDControlPoint;

USTRUCT()
struct FConstructionSlot
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TSubclassOf<class ATDUnit> Prototype;

	UPROPERTY()
	ATDSquad* Squad;

	UPROPERTY()
	float Progress;
};

UCLASS()
class ATDPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDPlayerController(const FObjectInitializer& ObjectInitializer);

	FORCEINLINE const TArray<ATDControlPoint*>& GetOwnedControlPoints() { return OwnedControlPoints; }

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Building)
	void BeginPlaceBuilding(TSubclassOf<ATDBuilding> buildingClass);

	void AcquireBuilding(ATDBuilding* building);

    UFUNCTION(BlueprintCallable, Category = BuildUnit)
    void ConstructSquad(TSubclassOf<class UTDRoster> Roster);

    void QueueConstruction(TSubclassOf<class ATDUnit> prototype, ATDSquad* squad, int32 count = 1);

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void ProcessPlayerInput(const float DeltaTime, const bool bGamePaused) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

private:

	void UpdateInputContext(float deltaTime);

	void AddInputContext(UTDInputContext* inputContext);
	void RemoveInputContext(UTDInputContext* inputContext);

	template<class TContextClass>
	void AddInputContext()
	{
		AddInputContext(NewObject<TContextClass>(this));
	}

	template<class THandler>
	void ProcessInputContextAction(const THandler& handler, bool canInterrupt);

	void OnSelect();
	
	void UpdateConstruction(float DeltaTime);
	void CompleteConstruction(TSubclassOf<class ATDUnit> prototype, ATDSquad* squad);

	void UpdateFocus();
	AActor* FindFocus();
	
	UPROPERTY(Transient)
	TArray<class ATDSquad*> Squads;

	UPROPERTY(Transient)
	TArray<class ATDBuilding*> Factories;

	UPROPERTY(Transient)
	TArray<FConstructionSlot> ConstructionQueue;

	UPROPERTY(Transient)
	AActor* Focused;

	UPROPERTY(Transient)
	TArray<UTDInputContext*> InputContextStack;

	UPROPERTY(Transient)
	TArray<ATDControlPoint*> OwnedControlPoints;
};


