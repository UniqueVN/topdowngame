// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "TDSquad.generated.h"

class ATDUnit;

UCLASS()
class TOPDOWNGAME_API ATDSquad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSquad();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetRoster(class UTDRoster* Roster);

	void Construct();

	void AddUnit(ATDUnit* unit);

	void SetFocused(bool focused);

private:

	UPROPERTY()
	class UTDRoster* UnitRoster;

	UPROPERTY()
	TArray<ATDUnit*> Units;
	
};
