// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "TDRoster.generated.h"

USTRUCT()
struct FRosterUnitInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATDUnit> Prototype;

	UPROPERTY(EditAnywhere, meta=(ClampMin = 0))
	int32 Count;
};

/**
 * 
 */
UCLASS(Blueprintable)
class TOPDOWNGAME_API UTDRoster : public UObject
{
	GENERATED_BODY()

public:
	const TArray<FRosterUnitInfo>& GetUnits() { return Units; }
	
private:

	UPROPERTY(EditAnywhere, Category = Roster)
	TArray<FRosterUnitInfo> Units;
	
};
