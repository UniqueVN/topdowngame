// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDSquad.h"
#include "TDPlayerController.h"
#include "TDRoster.h"
#include "OOB/TDUnit.h"


// Sets default values
ATDSquad::ATDSquad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDSquad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSquad::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ATDSquad::SetRoster(class UTDRoster* Roster)
{
	UnitRoster = Roster;
}

void ATDSquad::Construct()
{
	auto Player = Cast<ATDPlayerController>(GetOwner());
	if (Player != NULL && UnitRoster != NULL)
	{
		for (const FRosterUnitInfo& Info : UnitRoster->GetUnits())
		{
			Player->QueueConstruction(Info.Prototype, this, Info.Count);
		}
	}
}

void ATDSquad::AddUnit(ATDUnit* unit)
{
	unit->SetSquad(this);
	Units.Add(unit);
}

void ATDSquad::SetFocused(bool focused)
{
	for (ATDUnit* unit : Units)
	{
		unit->SetFocused(focused);
	}
}

