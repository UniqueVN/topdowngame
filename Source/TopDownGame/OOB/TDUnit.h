// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Focusable.h"
#include "TDUnit.generated.h"

class ATDSquad;

UCLASS()
class TOPDOWNGAME_API ATDUnit : public ACharacter, public IFocusable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDUnit();

public:

	UPROPERTY(VisibleDefaultsOnly)
	class UDecalComponent* SelectionIndicator;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = "true"))
	int32 ConstructionCost;

public:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	ATDSquad* GetSquad() const;
	void SetSquad(ATDSquad* squad);

    void SetOutlineVisibility(bool bVisible);
	void SetFocused(bool focused);

private:

	virtual void Focus(ATDPlayerController* player) override;
	virtual void Unfocus(ATDPlayerController* player) override;

private:
	
	UPROPERTY()
	ATDSquad* Squad;
	
};
