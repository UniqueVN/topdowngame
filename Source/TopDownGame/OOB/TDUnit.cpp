// Fill out your copyright notice in the Description page of Project Settings.

#include "TopDownGame.h"
#include "TDUnit.h"
#include "TDSquad.h"
#include "AIController.h"

// Sets default values
ATDUnit::ATDUnit():
	ConstructionCost(0)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SelectionIndicator = CreateDefaultSubobject<UDecalComponent>(TEXT("SelectionIndicator"));
	SelectionIndicator->RelativeLocation.Z = -GetCapsuleComponent()->GetScaledCapsuleHalfHeight() - 1.f;
	SelectionIndicator->RelativeRotation = FRotator(-90.f, 0.f, 0.f);
	SelectionIndicator->RelativeScale3D = FVector(4.f, 50.f, 50.f);
	SelectionIndicator->AttachTo(RootComponent);

	UCharacterMovementComponent* MovementComp = GetCharacterMovement();
	MovementComp->bOrientRotationToMovement = true; // Rotate character to moving direction
	MovementComp->RotationRate = FRotator(0.f, 640.f, 0.f);
	MovementComp->bConstrainToPlane = true;
	MovementComp->bSnapToPlaneAtStart = true;

	AutoPossessAI = EAutoPossessAI::Spawned;
	AIControllerClass = AAIController::StaticClass();
}

// Called when the game starts or when spawned
void ATDUnit::BeginPlay()
{
	Super::BeginPlay();

	SelectionIndicator->SetVisibility(false, true);
	
	FVector dest = GetActorLocation() + FMath::VRandCone(GetActorForwardVector(), FMath::DegreesToRadians(90.f)).GetSafeNormal2D() * FMath::FRandRange(200.f, 600.f);
	UNavigationSystem* const nav = GetWorld()->GetNavigationSystem();
	if (nav != NULL)
	{
		nav->SimpleMoveToLocation(GetController(), dest);
	}
}

// Called every frame
void ATDUnit::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ATDUnit::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

ATDSquad* ATDUnit::GetSquad() const
{
	return Squad;
}

void ATDUnit::SetSquad(ATDSquad* squad)
{
	check(Squad == NULL);
	Squad = squad;
}

void ATDUnit::Focus(ATDPlayerController* player)
{
	if (Squad != NULL)
	{
		Squad->SetFocused(true);
	}
}

void ATDUnit::Unfocus(ATDPlayerController* player)
{
	if (Squad != NULL)
	{
		Squad->SetFocused(false);
	}
}

void ATDUnit::SetFocused(bool focused)
{
	SelectionIndicator->SetVisibility(focused, true);
}

void ATDUnit::SetOutlineVisibility(bool bVisible)
{
    USkeletalMeshComponent* MeshComp = GetMesh();
    if (MeshComp)
        MeshComp->SetRenderCustomDepth(bVisible);
}


