// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/WorldSettings.h"
#include "TDWorldSettings.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API ATDWorldSettings : public AWorldSettings
{
	GENERATED_BODY()
	
public:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = World, AdvancedDisplay)
    UTexture2D* MiniMapTexture;
};
