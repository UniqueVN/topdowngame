// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TopDownGame.h"
#include "TDGameMode.h"
#include "TDPlayerController.h"
#include "Player/TDSpectatorPawn.h"
#include "Buildings/TDBuilding.h"

ATDGameMode::ATDGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDPlayerController::StaticClass();
	DefaultPawnClass = ATDSpectatorPawn::StaticClass();
	SpectatorClass = ATDSpectatorPawn::StaticClass();
}

void ATDGameMode::RestartPlayer(AController* NewPlayer)
{
	AActor* const StartSpot = FindPlayerStart(NewPlayer);
	if (StartSpot != NULL)
	{
		// initialize and start it up
		InitStartSpot(StartSpot, NewPlayer);
		NewPlayer->SetInitialLocationAndRotation(StartSpot->GetActorLocation(), StartSpot->GetActorRotation());
	}
	else
	{
		UE_LOG(LogTopDownGame, Warning, TEXT("Player start not found, failed to restart player"));
	}
}

void ATDGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	for (TActorIterator<ATDBuilding> it(GetWorld()); it; ++it)
	{
		ATDBuilding* building = *it;
		if (building->GetStartingOwnerIndex() == 0)
		{
			ATDPlayerController* player = Cast<ATDPlayerController>(GetWorld()->GetFirstPlayerController());
			if (player != NULL)
			{
				player->AcquireBuilding(building);
			}
		}

		building->BecomeActive();
	}
}