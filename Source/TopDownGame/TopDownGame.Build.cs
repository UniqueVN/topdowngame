// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TopDownGame : ModuleRules
{
	public TopDownGame(TargetInfo Target)
	{
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule" });
        PublicDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore", "UMG" });
        PublicDependencyModuleNames.AddRange(new string[] { "AIModule" });
    }
}
