#pragma once
#include "Blueprint/UserWidget.h"
#include "TDHUDIndicator.generated.h"

UCLASS(Blueprintable)
class UTDHUDIndicator : public UUserWidget
{
    GENERATED_BODY()

public:
    UTDHUDIndicator(const FObjectInitializer& ObjectInitializer);

public: // Editor Properties
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Content, meta = (AllowPrivateAccess = "true"))
    FString DisplayName;

public:
    // UWidget interface
    virtual void SynchronizeProperties() override;
    // End of UWidget interface

    FString GetDisplayName() const;
    void SetDisplayName(const FString& NewName);

protected:
    void UpdateDisplayName();
};

