// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TopDownGame.h"
#include "Blueprint/UserWidget.h"
#include "Components/BillboardComponent.h"
#include "Components/WidgetComponent.h"
#include "TDHUDOverlay.h"
#include "TDHUD.h"

ATDHUD::ATDHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
    HUDOverlay = nullptr;
}

void ATDHUD::DrawHUD()
{
    Super::DrawHUD();

    if (bShowHUD)
    {
        ShowHUDOverlay();
        if (HUDOverlay)
        {
            HUDOverlay->DrawHUD();
        }
    }
}

void ATDHUD::ShowHUD()
{
    Super::ShowHUD();

    if (bShowHUD)
    {
        ShowHUDOverlay();
    }
    else
    {
        HideHUDOverlay();
    }
}

void ATDHUD::ShowHUDOverlay()
{
    if (!HUDOverlayClass)
        return;

    if (HUDOverlay)
        return;

    HUDOverlay = CreateWidget<UTDHUDOverlay>(GetWorld(), HUDOverlayClass);
    if (HUDOverlay)
    {
        HUDOverlay->AddToViewport();
    }
}

void ATDHUD::HideHUDOverlay()
{
    if (!HUDOverlay)
        return;

    HUDOverlay->RemoveFromParent();
    HUDOverlay = nullptr;
}
