#pragma once
#include "Blueprint/UserWidget.h"
#include "TDHUDOverlay.generated.h"

UCLASS(Blueprintable)
class UTDHUDOverlay : public UUserWidget
{
    GENERATED_BODY()

public:
    UTDHUDOverlay(const FObjectInitializer& ObjectInitializer);

public: // Editor Properties
    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Content, meta = (AllowPrivateAccess = "true"))
    //FString DisplayName;

public:

    void DrawHUD();

protected:
};

