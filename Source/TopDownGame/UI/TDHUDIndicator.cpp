// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "TopDownGame.h"
#include "Blueprint/UserWidget.h"
#include "Components/BillboardComponent.h"
#include "Components/WidgetComponent.h"
#include "TDHUDIndicator.h"

UTDHUDIndicator::UTDHUDIndicator(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UTDHUDIndicator::SynchronizeProperties()
{
    Super::SynchronizeProperties();
    UpdateDisplayName();
}

FString UTDHUDIndicator::GetDisplayName() const
{
    return DisplayName;
}

void UTDHUDIndicator::SetDisplayName(const FString& NewName)
{
    DisplayName = NewName;
    UpdateDisplayName();
}

void UTDHUDIndicator::UpdateDisplayName()
{
    UTextBlock* txtName = Cast<UTextBlock>(GetWidgetFromName(FName(TEXT("txtName"))));
    if (txtName)
    {
        txtName->SetText(FText::FromString(DisplayName));
    }
}
