#pragma once
#include "Blueprint/UserWidget.h"
#include "GameFramework/HUD.h"
#include "TDHUDOverlay.h"
#include "TDHUD.generated.h"

UCLASS(Blueprintable)
class ATDHUD : public AHUD
{
    GENERATED_UCLASS_BODY()

public: // Override
    /** hides or shows HUD */
    virtual void ShowHUD() override;

    /** The Main Draw loop for the hud.  Gets called before any messaging.  Should be subclassed */
    virtual void DrawHUD() override;

public: // Editor properties
    UPROPERTY(EditAnywhere, Category = HUD)
    TSubclassOf<class UTDHUDOverlay> HUDOverlayClass;

protected:
    void ShowHUDOverlay();
    void HideHUDOverlay();

    UTDHUDOverlay* HUDOverlay;
};
