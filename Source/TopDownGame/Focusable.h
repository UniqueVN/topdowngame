#pragma once
#include "Focusable.generated.h"

class ATDPlayerController;

UINTERFACE()
class UFocusable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IFocusable
{
	GENERATED_IINTERFACE_BODY()

	virtual void Focus(ATDPlayerController* player) = 0;
	virtual void Unfocus(ATDPlayerController* player) = 0;
};