// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#ifndef __TOPDOWNGAME_H__
#define __TOPDOWNGAME_H__

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTopDownGame, Log, All);

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"

#define ECC_Placement ECC_GameTraceChannel1

#endif
